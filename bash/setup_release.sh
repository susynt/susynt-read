#!/bin/bash

#########################################################
# setup_release
#
# script to compile the packages picked up by the
# setup_area script
#
# daniel.joseph.antrim@cern.ch
# August 2017
#
########################################################

default_release="AnalysisBase,21.2.60,here"

function print_usage {
    echo "------------------------------------------------"
    echo " setup_release"
    echo ""
    echo " Options:"
    echo "  -r|--release             Set the AnalysisBase release [default: $default_release]"
    echo "  -c|--compile             Perform full compilation [default: false]"
    echo "  -h|--help                Print this help message"
    echo ""
    echo " Example usage:"
    echo " - First time setup and compilation:"
    echo "    $ source setup_release.sh --compile"
    echo " - Area is compiled, but starting from new shell:"
    echo "    $ source_setup_release.sh"
    echo "------------------------------------------------"
}

function setup_release {
    release=${1}
    dirname="./source/"
    startdir=${PWD}
    if [[ -d $dirname ]]; then
        cd $dirname
    else
        echo "setup_release    ERROR directory $dirname not found"
        return 1
    fi

    echo "setup_release    Setting up release: $release"
    export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    #export ATLAS_LOCAL_ASETUP_VERSION=current
    #export ATLAS_LOCAL_RCSETUP_VERSION=current
    lsetup "asetup $release"

    cd $startdir
}

function compile_sw {
    builddir="./build/"
    startdir=${PWD}
    if [[ -d $builddir ]]; then
        echo "setup_release    Removing old build directory"
        rm -rf $builddir
    fi
    mkdir -p $builddir
    cd $builddir
    echo "setup_release    Calling cmake"
    cmake ../source
    echo "setup_release    Calling make"
    make -j4
    cd $startdir
}

function setup_env {
    builddir="./build"
    startdir=${PWD}
    if [[ -d $builddir ]]; then
        cd $builddir
        founddir=0
        for f in *; do
            if [[ $f == *"x86_64"* ]]; then
                cd $f
                founddir=1
                if [[ -f "setup.sh" ]]; then
                    source setup.sh
                else
                    echo "setup_release    WARNING setup.sh file not found in ${PWD}"
                fi
            fi
        done
        if [[ $founddir == 0 ]]; then
            echo "setup_release    WARNING did not find x86_64-* directory in $builddir"
        fi
    else
        echo "setup_release    $builddir directory not found, cannot setup environment"
    fi
    cd $startdir
}

function main {

    release=$default_release
    compile=0

    while test $# -gt 0
    do
        case $1 in
            -r)
                release=${2}
                shift
                ;;
            --release)
                release=${2}
                shift
                ;;
            -c)
                compile=1
                ;;
            --compile)
                compile=1
                ;;
            -h)
                print_usage
                return 0
                ;;
            --help)
                print_usage
                return 0
                ;;
            *)
                echo "setup_release    ERROR Invalid argument: $1"
                return 1
                ;;
        esac
        shift
    done

    echo "setup_release    Starting -- `date`"
    startdir=${PWD}

    if ! setup_release $release ; then return 1; fi
    if [[ $compile == 1 ]]; then
        if ! compile_sw ; then return 1; fi
    fi
    if ! setup_env ; then return 1; fi
}

#_______________
main $*
